
package hello.models;

import org.springframework.data.repository.CrudRepository;

//esitellään DogDAO-interface joka käyttää CrudRepositoryn metodeita
//toiminnallsuuksia
public interface DogDAO extends CrudRepository<Dog, Integer> {

    //tähän voi esitellä omia haluamiaan metodeita mitä tarvitsee
    //hibernate, tai joku muu jätkä luo metodin nimellä tarvittavat toiminnallisuudet
    //tässä esimerkiksi parametrinä annettavalla nimellä palautetaan Dog-tyyppinen olio
    //ja joku muu luo toiminnallisuuden
    //tästä löytyy lisää tietoa myös netistä, esim mitä sanoja metodin nimessä voi käyttää
    //kätevää ku saippua
    public Dog findByNimi(String nimi);
}
