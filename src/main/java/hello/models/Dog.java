package hello.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//Sanotaan luokalle, että se on entity
@Entity
public class Dog implements Serializable {

    //Annotaatiot Id primary keylle, ja GeneratedValue automaattiselle luonnille
    @Id
    @GeneratedValue
    private int id;
    private String nimi;
    private String rotu;

    public Dog() {
    }

    public Dog(String nimi, String rotu) {
        this.nimi = nimi;
        this.rotu = rotu;
    }

    public int getId() {
        return id;
    }

    public String getNimi() {
        return nimi;
    }

    public String getRotu() {
        return rotu;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setRotu(String rotu) {
        this.rotu = rotu;
    }

}
