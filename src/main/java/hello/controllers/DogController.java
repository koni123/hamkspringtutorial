package hello.controllers;

import hello.models.Dog;
import hello.models.DogDAO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DogController {
    
    @Autowired
    private DogDAO dogDao;
    
    @RequestMapping(value = "/addDog", method = RequestMethod.POST)
    public String addDog() {
        Dog koira = new Dog("Murre", "Koulukoira");
        dogDao.save(koira);
        return "redirect:/hello";
    }
    
    @RequestMapping(value = "/listDog", method = RequestMethod.GET)
    public String listDog(Model model) {

        List<Dog> list = new ArrayList();
        list = (List<Dog>) dogDao.findAll();

        model.addAttribute("dogList", list);
        return "hello";
    }
    
}
